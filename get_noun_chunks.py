from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')


def get_continuous_chunks(text):
     chunked = ne_chunk(pos_tag(word_tokenize(text)))
     continuous_chunk = []
     current_chunk = []
     for i in chunked:
             if type(i) == Tree:
                     current_chunk.append(" ".join([token for token, pos in i.leaves()]))
             if current_chunk:
                     named_entity = " ".join(current_chunk)
                     if named_entity not in continuous_chunk:
                             continuous_chunk.append(named_entity)
                             current_chunk = []
             else:
                     continue
     return continuous_chunk



if __name__ == "__main__":
    text = "I want to know about the pune metro system. In particular, if the kothrud substation, it's traffic, capacity and daily iterarary."
    noun_phrases = get_continuous_chunks(text)
    print(f'Noun phrases: \n{noun_phrases}')