import pandas as pd
import numpy as np
import json
import re
import argparse
import sys
from pprint import pprint
from utils_main_new import preprocess_df, alias_intents, read_yaml

def alias_intents(df: pd.DataFrame, intent_col: str, alias_yaml: str):
    mapping = read_yaml(alias_yaml)
    if mapping:
        for i in range(df.shape[0]):
            for key in mapping:
                if df.at[i, intent_col] in mapping[key]:
                    df.at[i, intent_col] = key
                    break
    return df

def validate_training_df(df: pd.DataFrame, intent_col: str, intents_yaml:str, key:str, alias_yaml:str):

    print(df.shape)
    # Ensuring all labels have valid value
    df = df[df[intent_col] != None]
    df = df[df[intent_col] != '']

    # Alias Intents
    try:
        df = df.reset_index()
    except:
        pass
    df = alias_intents(df, intent_col, alias_yaml)


    mapping = read_yaml(intents_yaml)
    if not key in mapping.keys():
        print(f'{key}  not in yaml')
        raise Exception

    mandatory_intents = mapping[key]
    current_intents = df[intent_col].value_counts().keys()
    # If df does not contain any intent from mandatory_intents, raise Exception
    if len(set(mandatory_intents) - set(current_intents)) > 0:
        print('DF missing the following mandatory intents:', set(mandatory_intents) - set(current_intents))
        raise Exception

    df = df[df[intent_col].isin(mandatory_intents)]

    print(df.shape)

    return df

def merge_vasr_and_gasr_alternatives(transcript):

    transcript_ = "[]"

    if transcript:
        try:
            transcript_ = json.loads(transcript)
            if len(transcript_) == 2:
                # print(transcript_)
                transcript_ = transcript[1][:5] + transcript[0][:5]
            
            transcript_ = json.dumps(transcript,ensure_ascii=False)
        
        except Exception as e:
            print(e)
            if len(transcript) > 2:
                transcript_ = json.dumps([json.loads(transcript)])
            else:
                return json.dumps([])

    return transcript_


if __name__ == '__main__':
    """
    Preprocessing Steps:
    1. Remove null transcripts, labels
    2. Remove duplicates based on data_id and audio_url
    3. Alias Intents
    4. Keep mandatory intents
    5. Reset Index and Store in csv
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "gsheet_file")
    parser.add_argument("--data_col", help = "data_col")
    parser.add_argument("--tag_col", help = "tag_col")
    parser.add_argument("--output_csv", help = "output_csv")
    parser.add_argument("--alias_yaml", help = "alias_yaml")
    parser.add_argument("--intents_yaml", help = "intents_yaml")
    parser.add_argument("--keep_intents", help = "keep_intents")
    parser.add_argument("--merge_alternatives", help="merge_alternatives")
    parser.add_argument("--ignore_validation", help="ignore_validation")

    args = parser.parse_args()

    data_col = 'alternatives'
    tag_col = 'tag'
    merge_alternatives = False
    ignore_validation = False
    
    if args.data_col:
        data_col = args.data_col
    if args.tag_col:
        tag_col = args.tag_col
    if args.merge_alternatives:
        merge_alternatives = args.merge_alternatives
    if args.ignore_validation:
        ignore_validation = args.ignore_validation
    

    df = pd.read_csv(args.input_csv)
    
    print(df.shape)
    df = df.drop_duplicates(subset=['data_id'])
    print(df.shape)
    
    if merge_alternatives:
        df['alternatives'] = df['alternatives'].apply(lambda x: merge_vasr_and_gasr_alternatives(x))

    original_intents = set(df[tag_col].value_counts().keys())
    df = preprocess_df(
                            df, 
                            intent_col=tag_col,
                            remove_acoustic=False,
                            keep_cols=['data_id',data_col,tag_col,'state','audio_url'],
                            # keep_cols=['data_id',data_col,tag_col],
                            alias_yaml=args.alias_yaml
    )
    

    print(df.shape)
    # tag_col = 'labels'
    if not ignore_validation:
        df = validate_training_df(df, intent_col=tag_col, intents_yaml=args.intents_yaml, key=args.keep_intents, alias_yaml=args.alias_yaml )
    
    print(df.shape)

    print(f"\nFinal Intent Distribution:")
    pprint(f"Total Intents: {len(df[tag_col].value_counts())}")

    final_intents = set(df[tag_col].value_counts().keys())

    print(final_intents - original_intents)
    print(original_intents - final_intents)
    pprint(df[tag_col].value_counts())

    df.to_csv(args.output_csv, index=False)


    """
    Sample Usage:
    python /root/amey/depository/scripts/preprocess_dataset.py \
    --input_csv=/root/amey/slu-test/data/2.0.2/classification/datasets/test_hi.csv \
    --output_csv=/root/amey/slu-test/data/2.0.2/classification/datasets/test_hi_.csv \
    --alias_yaml=/root/amey/clients/vodafone/vodafone-dialogy-0.9.8-new/config/alias-train.yaml \
    --intents_yaml=/root/amey/clients/vodafone/vodafone-dialogy-0.9.8-new/config/intents.yaml \
    --keep_intents=mandatory_intents_train \
    --merge_alternatives=True \
    --ignore_validation=True \
    ;
    """