from utils_main import detect_devanagari
import argparse
import pandas as pd

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--output_csv", help = "output_csv")
    args = parser.parse_args()
    
        
    df = pd.read_csv(args.input_csv)
    print(f"Processing. Input File: {args.input_csv}, \n Output File: {args.output_csv}\n Total Data: {df.shape}")

    df['is_devanagri'] = df['alternatives'].apply(lambda x : detect_devanagari(x))
    df.to_csv(f"{args.output_csv}", index = False) if ".csv" in args.output_csv else df.to_csv(f"{args.output_csv}.csv", index = False)
    print(f"\nSaved to {args.output_csv}.")