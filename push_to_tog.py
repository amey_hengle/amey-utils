from togpush import fsm, tog


print("\n\nPushing data to TOG")

tog.push(client_id = 13, lang = "ta", days = 8, job_id = 1760,
         data_source = "FSM", priority = 1, call_quantity = 2000, turn_quantity = 1500)

tog.push(client_id = 13, lang = "te", days = 8, job_id = 1761,
         data_source = "FSM", priority = 1, call_quantity = 2000, turn_quantity = 1500)

tog.push(client_id = 13, lang = "hi", days = 8, job_id = 1762,
         data_source = "FSM", priority = 1, call_quantity = 2000, turn_quantity = 1500)

tog.push(client_id = 13, lang = "en", days = 8, job_id = 1763,
         data_source = "FSM", priority = 1, call_quantity = 2000, turn_quantity = 1500)