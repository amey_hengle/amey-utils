import json
import requests
import pandas as pd
from tqdm import tqdm
import argparse

def make_plute_request(row: dict):
    plute_request = {}
    if not any(['alternatives', 'state', 'call_uuid', 'conversation_uuid']) in row:
        raise Exception
    plute_request['alternatives'] = row['alternatives']
    plute_request['context'] = {
                                'call_uuid' : row ['call_uuid'],
                                'uuid' : row['conversation_uuid'],
                                'current_state' : row['state']
    }
    return plute_request

def make_slu_response(slu_response : str):
    r = {}
    slu_response = json.loads(slu_response)
    r['prediction'] = json.dumps(slu_response['intents'], ensure_ascii=False)
    r['predicted_intent'] = json.dumps(slu_response['intents']['name'], ensure_ascii=False)
    r['predicted_entities'] = json.dumps(slu_response['intents']['slots'], ensure_ascii=False)

    return r


def make_prediction_pipeline(df: pd.DataFrame, target_url:str):
    resuld_df = pd.DataFrame(columns=['id', 'data_id', 'prediction', 'predicted_intent', 'predicted_entities'])
    print(f'\nTotal Data: {df.shape} rows')

    for i in tqdm(range(df.shape[0])):
        data_id = df.iloc[i]['data_id']
        id = df.iloc[i]['id']
        plute_request = make_plute_request(df.iloc[i])

        try:
            response = requests.post(url=target_url, json=plute_request)
            slu_prediction = make_slu_response(response.text)
            if slu_prediction:
                slu_prediction['data_id'] = data_id
                slu_prediction['id'] = id
                resuld_df = resuld_df.append(slu_prediction, index=False)
        except Exception as e:
            print(e)
            continue

    return resuld_df


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--output_csv", help = "output_csv")
    parser.add_argument("--target_url", help = "target_url")
    args = parser.parse_args()

    df = pd.read_csv(args.input_csv)
    resuld_df = make_prediction_pipeline(df, target_url=args.target_url)
    if resuld_df:
        resuld_df.to_csv(args.output_csv, index=False)
        print(f'\nDataFrame stored to {args.output_csv}')
    else:
        print('\nDataFrame save error')