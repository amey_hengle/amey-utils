import pandas as pd
import argparse
import json
from utils_main_new import read_multiple_df


missing_cols = set()
def unpack(df):
  for i, row in df.iterrows():
    d = json.loads(row["data"])
    if isinstance(d,str):
      d = json.loads(d)

    for k, v in d.items():
      if k not in df.columns:
        missing_cols.add(k)

  for col in missing_cols:
    df[col] = None

  for i, row in df.iterrows():
    d = json.loads(row["data"])
    if isinstance(d,str):
      d = json.loads(d)
    
    for k, v in d.items():
        df.at[i,k] = v

  return df

def get_tag(tag):
    if tag:
        if not isinstance(tag,str):
            print(tag)
        else:
            tag = json.loads(tag)
            if isinstance(tag, str):
              tag = json.loads(tag)
            tag = tag[0]['type']
        return tag
    else:
        return None

def get_prediction(prediction):
    if prediction:
        if not isinstance(prediction,str):
            print(prediction)
        else:
            prediction = json.loads(prediction)
        # print(prediction)
            prediction = prediction['intents'][0]['name']
        return prediction
    else:
        return None

def fix_alternatives(alternatives):
  if alternatives:
    if not isinstance(alternatives,str):
      try:
        return json.dumps(alternatives)
      except:
        print(alternatives)
  return alternatives

def preprocess_oppo(df):
  acoustic_oos = ['_unknown_','_dtmf_,''_partial_', 'audio_silent', 'audio_noisy',  'audio_channel_noise', 'audio_channel_noise_hold', 'audio_speech_unclear', 'audio_speech_volume', 'audio_silent', 'background_noise', 'background_speech', 'other_language', 'acoustic_oos', 'silence']
  drop_indexes = []
  
  for i in range(df.shape[0]):
    # If predicted_intent is None
    if df.iloc[i].intent == None or not isinstance(df.iloc[i].intent,str):
      # IF true value belongs to acoustic oos
      if df.iloc[i].tag in acoustic_oos:
          drop_indexes.append(i)

  if len(drop_indexes) > 0:
    print(f"Dropping {len(drop_indexes)} non-prediction utterances")
    df = df.drop(drop_indexes, axis = 0)

  df = df.reset_index()
  return df


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--input_folder", help = "input_folder")
    parser.add_argument("--output_csv", help = "output_csv")

    args = parser.parse_args()
    df = pd.DataFrame()

    if args.input_csv:
        df = pd.read_csv(args.input_csv)
    elif args.input_folder:
        df = read_multiple_df(args.input_folder)
    else:
        raise Exception
    
    if 'level_0' in df.columns:
        df_main = df.drop(['level_0'],axis=1)
    
    df = unpack(df)
    df['tag'] = df['tag'].apply(lambda x : get_tag(x))
    df['intent'] = df['prediction'].apply(lambda x : get_prediction(x))
    df['alternatives'] = df['alternatives'].apply(lambda x : fix_alternatives(x))
    df.to_csv(args.output_csv, index=False)


"""
Sample usage:

python \
    /root/amey/depository/scripts/kubeflow-df-preprocess.py \
    --input_folder=/root/amey/depository/temp \
    --output_csv=/root/amey/depository/datasets/csv/vodafone/vf_4228_hi_.csv

"""