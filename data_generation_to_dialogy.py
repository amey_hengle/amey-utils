from utils_main import data_generation_to_dialogy
import argparse

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_sqlite", help = "input_sqlite")
    parser.add_argument("--output_csv", help = "output_csv")
    args = parser.parse_args()
    
    print(f"Converting to Dialogy. Input File: {args.input_sqlite}, \n Output File: {args.output_csv}")
    df_dialogy = data_generation_to_dialogy(args.input_sqlite)
    df_dialogy.to_csv(f"{args.output_csv}", index = False) if ".csv" in args.output_csv else df_dialogy.to_csv(f"{args.output_csv}.csv", index = False)
    print(f"\nSaved to {args.output_csv}.")