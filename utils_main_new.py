import pandas as pd
import sqlite3
import json
import uuid
from langdetect import detect
import yaml
from ast import literal_eval
import glob
import unicodedata
import re
import string

lexical_oos = ['_oos_', '_ood_', 'other_intent', 'lexical_oos']
acoustic_oos = ['_dtmf_,''_partial_', 'audio_silent', 'audio_noisy',  'audio_channel_noise', 'audio_channel_noise_hold', 'audio_speech_unclear', 'audio_speech_volume', 'audio_silent', 'background_noise', 'background_speech', 'other_language', 'acoustic_oos', 'silence']
null = [None, 'null', 'nan']
punctuation = '''`÷×؛<>_()*&^%][ـ،/:"؟.,'{}~¦+|!”…“–ـ''' + string.punctuation


def multiply_df(df: pd.DataFrame, factor: int):
    df = pd.concat([df]*factor, ignore_index=True)
    df["data_id"] = [str(uuid.uuid4()) for _ in range(df.shape[0])]
    df = df.reset_index()
    return df

def clean_special_characters(sample):
    cleaned = ""
    for word in sample:
        for character in word:
            if not character in punctuation:
                cleaned = cleaned + character
            else:
                cleaned = cleaned + ' '
        else:
            continue

    return cleaned.strip()


def clean_text(text):
    """
    Function for cleaning a string.
        @param text (string): text to be cleaned.
        @return text (string): cleaned string. 
    """
    # normalization 1: xxxThis is a --> xxx. This is a (missing delimiter)
    text = re.sub(r'([a-z])([A-Z])', r'\1\. \2', text)  # before lower case
    # normalization 2: lower case
    text = text.lower()
    # normalization 3: "&gt", "&lt"
    text = re.sub(r'&gt|&lt', ' ', text)
    # normalization 4: letter repetition (if more than 2)
    text = re.sub(r'([a-z])\1{2,}', r'\1', text)
    # normalization 5: non-word repetition (if more than 1)
    text = re.sub(r'([\W+])\1{1,}', r'\1', text)
    # normalization 6: string * as delimiter
    text = re.sub(r'\*|\W\*|\*\W', '. ', text)
    # normalization 7: stuff in parenthesis, assumed to be less informal
    text = re.sub(r'\(.*?\)', '. ', text)
    # normalization 8: xxx[?!]. -- > xxx.
    text = re.sub(r'\W+?\.', '.', text)
    # normalization 9: [.?!] --> [.?!] xxx
    text = re.sub(r'(\.|\?|!)(\w)', r'\1 \2', text)
    # normalization 10: ' ing ', noise text
    text = re.sub(r' ing ', ' ', text)
    # normalization 12: phrase repetition
    text = re.sub(r'(.{2,}?)\1{1,}', r'\1', text)

    text = clean_special_characters(text)

    return text.strip()


def alias_intents(df: pd.DataFrame, intent_col: str, alias_yaml: str):
    # df = df.reset_index()
    mapping = read_yaml(alias_yaml)
    if mapping:
        for i in range(df.shape[0]):
            for key in mapping:
                try:
                    if df.at[i, intent_col] in mapping[key]:
                        df.at[i, intent_col] = key
                        break
                except:
                    continue
    return df

def read_multiple_df(input_directory, usecols = []):
    
    # usecols = ['data_id', 'alternatives', 'tag', 'audio_url','intent']

    df_main = pd.DataFrame()    
    df_main['lang'] = 'unknown'
    for filename in glob.iglob(f'{input_directory}/*'):
        try:
            lang = 'unknown'
            if '_en' in filename:
                lang = 'en'
            if '_hi' in filename:
                lang = 'hi'
            if '_ta' in filename:
                lang = 'ta'
            if '_te' in filename:
                lang = 'te'
            
            if usecols:
                df = pd.read_csv(filename, usecols=usecols)
            else:
                df = pd.read_csv(filename)
                
            df['lang'] = lang
            df_main = df_main.append(df)
        except Exception as e:
            
            print(e, filename)
    return df_main

def data_generation_to_dialogy(filename, standardize = False,  audio_prefix = 's3://tog-call-data-generation/'):
    
    df_sqlite = pd.DataFrame()
    df_dialogy = pd.DataFrame(columns=["data_id", "data", "labels"])

    if filename.endswith(".sqlite"):
        
        # Reading data from sqlite
        connection_sqlite = sqlite3.connect(filename)
        df_sqlite = pd.read_sql_query("SELECT * FROM data", connection_sqlite)
        
        # Getting data to dialogy format
        df_dialogy.data_id = df_sqlite.data_id
 
        df_dialogy.data = df_sqlite.data.apply(lambda x : json.dumps(json.loads(x)["prediction"]["graph"]["input"][0]))
 
        df_dialogy.labels = df_sqlite.tag.apply(lambda x : json.loads(x)[0]['type'])
 
        df_dialogy['conversation_id'] = df_sqlite.data.apply(lambda x :json.loads(x)['conversation_id'])
 
        df_dialogy['call_id'] = df_sqlite.data.apply(lambda x :json.loads(x)['call_id'])
 
        df_dialogy['state'] = df_sqlite.data.apply(lambda x :json.loads(x)['state'])
 
        df_dialogy['alternatives'] = df_sqlite.data.apply(lambda x : json.dumps(json.loads(x)['alternatives'], ensure_ascii=False))
 
        df_dialogy['audio_url'] = df_sqlite.data.apply(lambda x :audio_prefix + json.loads(x)['audio_url'])
 
        df_dialogy['uuid'] = df_sqlite.data.apply(lambda x :json.loads(x)['uuid'])
 
        df_dialogy['reftime'] = df_sqlite.data.apply(lambda x :json.loads(x)['reftime'])

        df_dialogy['prediction'] = df_sqlite.data.apply(lambda x :json.loads(x)['prediction'])
        
        try:
           df_dialogy['call_duration'] = df_sqlite.data.apply(lambda x :json.loads(x)['call_duration'])
        except:
            df_dialogy['call_duration'] = ''
        try:
            df_dialogy['state_transitions'] = df_sqlite.data.apply(lambda x :json.loads(x)['state_transitions'])
        except:
            df_dialogy['state_transitions'] = 'COF -> COF -> COF'
        try:
            df_dialogy['intent'] = df_sqlite.data.apply(lambda x :json.loads(x)['intent'])
        except:
            df_dialogy['intent'] = '_oos_'
        try:
            df_dialogy['transcript_len'] = df_sqlite.data.apply(lambda x :json.loads(x)['transcript_len'])
        except:
            df_dialogy['transcript_len'] = 1
            
                    
        # Removing null transcripts
        df_dialogy["null_transcript"] = df_dialogy.data.apply(lambda x : False if json.loads(x) else True)
        df_dialogy = df_dialogy[df_dialogy["null_transcript"] == False]
        df_dialogy = df_dialogy.drop(columns=["null_transcript"])

        # Get intent names in standard format (optional): Converts intent_name -> _intent_name_
        if standardize:
            df_dialogy.labels = df_dialogy.labels.apply(lambda x : standardize_intent_name(x))
            df_dialogy = df_dialogy[df_dialogy.labels != "nan"]

        return df_dialogy
    
    else:
        raise Exception

def convert_to_transciption_tagging(df):
    colnames = ['call_uuid','conversation_uuid','alternatives','audio_url','reftime','prediction','state','call_duration','state_transitions','intent','transcript_len']
    df.rename(columns={'call_id' : 'call_uuid', 'conversation_id' : 'conversation_uuid'}, inplace=True)
    df = df[colnames]
    return df


def charon_to_dialogy(filename, standardize = True):
    try:
        df_charon = pd.read_csv(filename)
    except:
        raise Exception
    
    if not 'data_id' in df_charon.columns:
        df_charon['data_id'] = [str(uuid.uuid4()) for _ in range(df_charon.shape[0])]
    
    df_dialogy = pd.DataFrame(columns=["data_id", "data", "labels"]) 

    df_dialogy.data_id = df_charon.data_id
    df_dialogy.labels = df_charon.intent
    df_dialogy.data = df_charon.alternatives

    # Removing null transcripts
    df_dialogy["null_transcript"] = df_dialogy.data.apply(lambda x : False if check_null_transcript(x) else True)
    
    df_dialogy = df_dialogy[df_dialogy["null_transcript"] == False]
    df_dialogy = df_dialogy.drop(columns=["null_transcript"])
    
    # Get intent names in standard format (optional): Converts intent_name -> _intent_name_
    if standardize:
        df_dialogy.labels = df_dialogy.labels.apply(lambda x : standardize_intent_name(x))
        df_dialogy = df_dialogy[df_dialogy.labels != "nan"]
        
    return df_dialogy


def check_null_transcript(transcript, print_nan=False):
    try:
        if transcript:
            if json.loads(transcript)[0]:
                return False
        
        if print_nan:
            print(f"Null Transcript: {str(transcript)}")
        
        return True
    
    except Exception as e:
        return True
    
def detect_lang(transcript):
    try:
        lang = None

        if transcript:
            transcript = json.loads(transcript)
            if isinstance(transcript,str):
                transcript = json.loads(transcript)
            if isinstance(transcript, list):
                if isinstance(transcript[0],str):
                    lang = detect(transcript[0].replace("</s>"," "))
                elif isinstance(transcript[0],list):
                    lang = detect(transcript[0][0].replace("</s>"," "))

            if lang:
                return lang

        return lang    
    except Exception as e:
        print(transcript)
        print(e)
        raise Exception

def dialogy_to_xlingual(transcript):
    try:
        alternatives = json.loads(transcript)[0]
        if not alternatives:
            return None
        else:
            # return " </s> </s> ".join([x['transcript'] for x in alternatives])
            alternatives = [x['transcript'] for x in alternatives]
            return "<s> " + " </s> <s> ".join(alternatives) + " </s>"
    except:
        return None


def get_predictions_from_json(prediction):
    if isinstance(prediction, str):
        try:
            p = json.loads(prediction)['intents'][0]['name']
            return p
        except:
            try:
                p = json.loads(prediction)['name']
                return p
            except:
                return "nan"
    else:
        return str(prediction)


def standardize_intent_name(intent, is_entity = False):
    
    if isinstance(intent, str):
        intent = intent.lower()
        if intent == "OOD":
            return "_ood_"
        
        if is_entity:
            intent = [x for x in intent.split() if len(x)!=0]
        else:
            intent = [x for x in intent.split("_") if len(x)!=0]
            
        # intent_fixed = "_" + "_".join([x for x in intent]) + "_"
        intent_fixed = "_".join([x for x in intent])
        
        if len(intent_fixed.split("_")) <= 1 and not is_entity:
            intent_fixed = "_" + intent_fixed + "_"
            
        else:
            if intent_fixed in ['request_agent', 'who_are_you']:
                intent_fixed = "_" + intent_fixed + "_"        
                    
        return intent_fixed
    else:
        return str(intent)


def get_proper_json_format(data):
    try:
        data = json.dumps(json.loads(data.replace("'", '"')), ensure_ascii=False)
        return data
    except Exception as e:
        # print(data, e)
        return None
    

def avg_transcript_len(transcript, roundoff=False):
    try:
        transcript = json.loads(transcript)[0]
        total_l = 0
        cnt = 0
        for _ in transcript:
            t_l = len( _['transcript'].split())
            if t_l != 0:
                total_l += t_l
                cnt += 1
        
        if cnt == 0:
            return 0
        else:
            if roundoff:
                return round(total_l / cnt)
            else:
                return float(total_l / cnt)
    except:
        return 0    

def undersample_by_label(df, label, num_points, label_col = "labels", data_col = "alternatives", random_state = 42):
    df_sampled = df[df[label_col] == label].sample(n = num_points, random_state = random_state)
    df = df[df[label_col] != label]
    df = df.append(df_sampled, ignore_index = True)
    # df = df.reset_index(drop=True)
    return df


    
def check_transcript_noise(transcript, keywords, print_noise=False):
    try:
        transcript = json.loads(transcript)[0]
        for _ in transcript:
            if any(x in keywords for x in  _['transcript'].split()):
                if print_noise:
                    print(transcript)
                
                return True
            
        return False
    
    except Exception as e:
        print(e)
        return False

def preprocess_df(df, intent_list = [], intent_col="intent", data_col = "alternatives", remove_acoustic = True, 
                  min_support = 10, print_nan=False, fill_data_id = False, standardize_intents = False, keep_cols = None, alias_yaml = '', fill_asr_type = True):

    df.rename(columns = {intent_col : "tag", data_col : "alternatives"}, inplace = True)

    if fill_data_id:
        df["data_id"] = [str(uuid.uuid4()) for _ in range(df.shape[0])]

    # df = df[["data_id","alternatives","tag"]]

    print(f"Shape before: {df.shape[0]} \n Total intents {len(df.tag.value_counts())}")

    df = df[df.alternatives.notna()]

    if standardize_intents:
        df.tag = df.tag.apply(lambda x : standardize_intent_name(x))

    df.alternatives = df.alternatives.apply(lambda x : get_proper_json_format(x))

    df["null_transcript"] = df.alternatives.apply(lambda x : check_null_transcript(x, print_nan=False))

    if print_nan:
        print(f"\nNull Transcripts: {str(df[df.null_transcript == True].alternatives.values.tolist())}")

    df = df[df.null_transcript == False]

    df['avg_transcript_len'] = df.alternatives.apply(lambda x : avg_transcript_len(x))

    # If Alias Yaml is passed, process tag column
    if len(alias_yaml) > 0:
        if isinstance(alias_yaml, str):
            try:
                df = df.reset_index()
            except:
                pass
            df = alias_intents(df, 'tag', alias_yaml=alias_yaml)

    if intent_list and len(intent_list) > 0:
        df = df[df.tag.isin(intent_list)]
    
    if keep_cols:
        df = df[keep_cols]
    
    if remove_acoustic:
        df = df[~df.tag.isin(acoustic_oos)]
    
    print(f"Shape after: {df.shape[0]}\n Total intents {len(df.tag.value_counts())}")

    v_counts = {k : v for k,v in zip(list(df.tag.value_counts().keys()), list(df.tag.value_counts()))}
    for k in v_counts:
        if v_counts[k] < min_support:
            df = df[df.tag != k]
    
    
    # Removing Duplicate Columns
    df = df.loc[:,~df.columns.duplicated()]
    # df.reset_index(inplace=True)
    
    if fill_asr_type:
        df['asr_type'] = df['alternatives'].apply(lambda x : detect_asr_type(x))

    return df


def fill_data_id(df):
    df["data_id"] = [str(uuid.uuid4()) for _ in range(df.shape[0])]    
    return df
    
    
def read_yaml(filename):
    try:
        data = None
        with open(filename) as file:
            data = yaml.load(file, Loader=yaml.FullLoader)
        if data:
            return data
        else:
            return None
    except Exception as e:
        print(e)
        return None

def save_yaml(dict_obj, filename):
    try:
        with open(filename, 'w', encoding='utf-8') as file:
            documents = yaml.dump(dict_obj, file, allow_unicode=True)
    except Exception as e:
        print(e)
        raise Exception

def detect_asr_type(transcript):
    try:
        transcripts = json.loads(transcript)[0]
        if transcripts and len(transcripts) > 0:
            if 'am_score' in transcripts[0]:
                return 'VASR'
            else:
                return 'GASR'
    except Exception as e:
        # print(e,transcript)
        return None


def detect_devanagari(transcript):

    if transcript:
        transcript = json.loads(transcript)

        if isinstance(transcript[0], list):
            transcripts = transcripts[0]
        else:
            transcripts = transcript
    try:
        for t in transcripts:
            _ = t['transcript']
            try:
                if _:
                    for i in _:
                        # print(i)
                        if "DEVANAGARI" in unicodedata.name(i):
                            return True
            except:
                continue
        return False
    except Exception as e:
        print(e,transcript)
        return None
    
"""
Extracts predicted_intent from predictions column in charon csv format. 
"""
def get_predictions(prediction):
    prediction = str(prediction)
    
    if isinstance(prediction, str):
        try:
            prediction = literal_eval(prediction)['name']
            if prediction:
                return(prediction)
        except Exception as e:
            try:
                prediction = json.loads(prediction)['name']
                return str(prediction)
            except:
                return str(prediction)
    else:
        return str(prediction)

def get_prediction_confidence(prediction : str):
    try:
        prediction = json.loads(prediction)
        if isinstance(prediction, list):
            cscore = float(prediction[0]['score'])
        elif isinstance(prediction, dict):
            cscore = float(prediction['score'])
        else:
            cscore = None
        return cscore
    except Exception as e:
        print(e)
        return None
    

# Convert Normal test to dialogy format
"""
1. Convert normal text to format [["transcript" : "abcdef"]]
"""
def string_to_alternatives(text: str):
    if text:
        if len(text) > 0:
            dialogy_format = [[{'transcript' : text}]]
            return json.dumps(dialogy_format, ensure_ascii=False)
    return None

def make_alternatives(df: pd.DataFrame, colname: str, fill_tag = '', return_df = True, savepath : str = ''):
    if colname not in df.columns:
        raise Exception
    else:
        df['alternatives'] = df[colname].apply(lambda x : string_to_alternatives(x))
        if fill_tag != '':
            df['tag'] = fill_tag
        df["data_id"] = [str(uuid.uuid4()) for _ in range(df.shape[0])]
        df =  df.reset_index(drop=True)

    if savepath != '':
        df.to_csv(savepath, index=False)
    
    if return_df:
        return df


def list_alternatives(alternatives : str, merge = False):
    if alternatives:
        alternatives = json.loads(alternatives)
        if len(alternatives) > 0:
            loose_canon = []
            for _ in alternatives[0]:
                loose_canon.append(_['transcript'])
            
            if merge:
                loose_canon = '</s> ' + ' </s> '.join([x for x in loose_canon]) + ' </s>'

            return loose_canon
    
    return []
#-----------------------------------------------------------------------#

#--------------------------------------------------------------------------------------------------------------#
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def make_confusion_matrix(cf,
            group_names=None,
            categories='auto',
            count=True,
            percent=True,
            cbar=True,
            xyticks=True,
            xyplotlabels=True,
            sum_stats=True,
            figsize=None,
            cmap='Blues',
            title=None):
    '''
    This function will make a pretty plot of an sklearn Confusion Matrix cm using a Seaborn heatmap visualization.
    Arguments
    ---------
    cf:            confusion matrix to be passed in
    group_names:   List of strings that represent the labels row by row to be shown in each square.
    categories:    List of strings containing the categories to be displayed on the x,y axis. Default is 'auto'
    count:         If True, show the raw number in the confusion matrix. Default is True.
    normalize:     If True, show the proportions for each category. Default is True.
    cbar:          If True, show the color bar. The cbar values are based off the values in the confusion matrix.
                   Default is True.
    xyticks:       If True, show x and y ticks. Default is True.
    xyplotlabels:  If True, show 'True Label' and 'Predicted Label' on the figure. Default is True.
    sum_stats:     If True, display summary statistics below the figure. Default is True.
    figsize:       Tuple representing the figure size. Default will be the matplotlib rcParams value.
    cmap:          Colormap of the values displayed from matplotlib.pyplot.cm. Default is 'Blues'
                   See http://matplotlib.org/examples/color/colormaps_reference.html
                   
    title:         Title for the heatmap. Default is None.
    '''


    # CODE TO GENERATE TEXT INSIDE EACH SQUARE
    blanks = ['' for i in range(cf.size)]

    if group_names and len(group_names)==cf.size:
        group_labels = ["{}\n".format(value) for value in group_names]
    else:
        group_labels = blanks

    if count:
        group_counts = ["{0:0.0f}\n".format(value) for value in cf.flatten()]
    else:
        group_counts = blanks

    if percent:
        group_percentages = ["{0:.2%}".format(value) for value in cf.flatten()/np.sum(cf)]
    else:
        group_percentages = blanks

    box_labels = [f"{v1}{v2}{v3}".strip() for v1, v2, v3 in zip(group_labels,group_counts,group_percentages)]
    box_labels = np.asarray(box_labels).reshape(cf.shape[0],cf.shape[1])


    # CODE TO GENERATE SUMMARY STATISTICS & TEXT FOR SUMMARY STATS
    if sum_stats:
        #Accuracy is sum of diagonal divided by total observations
        accuracy  = np.trace(cf) / float(np.sum(cf))

        #if it is a binary confusion matrix, show some more stats
        if len(cf)==2:
            #Metrics for Binary Confusion Matrices
            precision = cf[1,1] / sum(cf[:,1])
            recall    = cf[1,1] / sum(cf[1,:])
            f1_score  = 2*precision*recall / (precision + recall)
            stats_text = "\n\nAccuracy={:0.3f}\nPrecision={:0.3f}\nRecall={:0.3f}\nF1 Score={:0.3f}".format(
                accuracy,precision,recall,f1_score)
        else:
            stats_text = "\n\nAccuracy={:0.3f}".format(accuracy)
    else:
        stats_text = ""


    # SET FIGURE PARAMETERS ACCORDING TO OTHER ARGUMENTS
    if figsize==None:
        #Get default figure size if not set
        figsize = plt.rcParams.get('figure.figsize')

    if xyticks==False:
        #Do not show categories if xyticks is False
        categories=False


    # MAKE THE HEATMAP VISUALIZATION
    plt.figure(figsize=figsize)
    sns.heatmap(cf,annot=box_labels,fmt="",cmap=cmap,cbar=cbar,xticklabels=categories,yticklabels=categories)

    if xyplotlabels:
        plt.ylabel('True label')
        plt.xlabel('Predicted label' + stats_text)
    else:
        plt.xlabel(stats_text)
    
    if title:
        plt.title(title)
        
#--------------------------------------------------------------------------------------------------------------#
