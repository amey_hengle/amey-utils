import pandas as pd
import ast
import Levenshtein as lev
from collections import Counter
import argparse
import yaml

def save_yaml(dict_obj, filename):
    try:
        with open(filename, 'w', encoding='utf-8') as file:
            documents = yaml.dump(dict_obj, file, allow_unicode=True)
    except Exception as e:
        print(e)
        raise Exception

def get_tag(tag):
    try:
        if not tag:
            return None
        tag = ast.literal_eval(tag)
        if tag:
            return tag[0]['type'].lower()
        else:
            return None
    except:
        return tag.lower()

def get_text(tag):
    try:
        if not tag:
            return None
        tag = ast.literal_eval(tag)
        if tag:
            return tag[0]['text'].lower()
        else:
            return None
    except:
        return tag.lower()


def parse_region_tags(df, tag_colname = 'tag'):
    entity_mappings = {}
    
    tags = df[tag_colname].values.tolist()
    
    for tag in tags:
        tag = ast.literal_eval(tag)

        if isinstance(tag, list):
            for _ in tag:
                text = _['text']
                type = _['type']
                # print(text,":",type)
                
                # Removing spaces after last character (Frequent Reggion tagging error)
                if text and text[-1] == ' ': 
                    text = text[:-1]
                # Removing spaces before first character (Frequent Reggion tagging error)
                if text and text[0] == ' ': 
                    text = text[1:]
                
                text = text.strip()

                if text and text in entity_mappings.keys():
                    
                    try:
                        # entity_mappings[text].add(type)                    
                        entity_mappings[text].append(type)
                        
                    except Exception as e:
                        print(e, "\n", text, type)
                        continue
                else:
                    if text and type:
                        # entity_mappings[text] = set([type])
                        entity_mappings[text] = [type]
                    
    return entity_mappings


def get_noisy_patterns(entity_mappings, threshold = 0.2):
    output = {}

    for _ in entity_mappings:
        frequency = Counter(entity_mappings[_]).most_common(1)[0][1]
        entity = Counter(entity_mappings[_]).most_common(1)[0][0].lower()
        variation = _.lower()
        # print(f"{entity} : {variation} : {Counter(entity_mappings[_])}")
        
        if entity in output:
            output[entity].add(variation)
        else:
            output[entity] = set([variation])

        output_optimized = {}
        for k in output:
            for v in output[k]:
                s = lev.ratio(k.lower(), v.lower())
                if s >= threshold:
                    if k in output_optimized:
                        output_optimized[k].append(v)
                    else:
                        output_optimized[k] = [v]
                
    return output_optimized


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--output_yaml", help = "output_yaml")
    parser.add_argument("--threshold", help = "threshold")
    args = parser.parse_args()

    threshold = 0.2
    if not args.threshold:
        threshold = 0.2

    try:
        df_main = pd.read_csv(args.input_csv)
        df_main = df_main[['alternatives','tag']]
        df_main['entity'] = df_main.tag.apply(lambda x : get_tag(x))

        entity_mappings = parse_region_tags(df_main)
        result = get_noisy_patterns(entity_mappings)

        if result:
            if isinstance(result, dict):
                save_yaml(result, args.output_yaml)

        else:
            raise Exception

    except Exception as e:
        print(e)
        raise Exception


"""
Sample usage:
python /root/amey/depository/scripts/noisy-patterns.py --input_csv=/root/amey/depository/datasets/MLWR/tagged_data.csv --output_yaml=/root/amey/depository/datasets/MLWR/output.yaml

"""