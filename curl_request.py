import json
import requests
alternatives = {
       "alternatives": [
            [
	        {
				"confidence": 0.88199484,
				"transcript": "हां मैं हिंदी में बात करना चाहता हूं"
			},
			{
				"confidence": 0.8666364,
				"transcript": "हमें हिंदी में बात करना चाहता हूं"
			},
			{
				"confidence": 0.7963935,
				"transcript": "हा मैं हिंदी में बात करना चाहता हूं"
			},
			{
				"confidence": 0.727421,
				"transcript": "हम हिंदी में बात करना चाहता हूं"
			},
			{
				"confidence": 0.7969339,
				"transcript": "हां में हिंदी में बात करना चाहता हूं"
			}
            ]
        ],
        "context": {
            "ack_slots": [],
            "asr_provider": "google",
            "bot_response": "नमस्कार! मैं वीआई से जेनेट हूं। कृपया अपनी पसंदीदा भाषा चुनें",
            "call_uuid": "e8ad7b72-b502-4bf2-8094-524206462d08",
            "current_intent": "",
            "current_state": "PREFERRED_LANGUAGE_STATE",
            "uuid": "bf2d2d91-e4bc-4b37-845c-ce43365524dd",
            "virtual_number": "918045389836"
        }
}
"""
plute_request = {
        "alternatives": [
            [
                {
                    "confidence": 0.8029602,
                    "transcript": "मुझे कल 5:00 से 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.799338,
                    "transcript": "मुझे कल 5:00 तक 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.8083113,
                    "transcript": "मुझे कल 5:00 से 5:00 से 6:00 बजे तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.7676532,
                    "transcript": "मुझे ना कल 5:00 से 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.76433295,
                    "transcript": "मुझे ना कल 5:00 तक 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.8254566,
                    "transcript": "मुझे आ कल 5:00 से 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.80499095,
                    "transcript": "मुझे कल 5:00 तक 5:00 से 6:00 बजे तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.8221362,
                    "transcript": "मुझे आ कल 5:00 तक 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.8593594,
                    "transcript": "मुझे कल 5:00 के 5:00 से 6:00 तक का बुकिंग चाहिए"
                },
                {
                    "confidence": 0.77530867,
                    "transcript": "मुझे ना कल 5:00 से 5:00 से 6:00 बजे तक का बुकिंग चाहिए"
                }
            ]
        ],
        "context": {
            "ack_slots": [],
            "asr_provider": "google",
            "bot_response": "<speak><prosody rate=\"93%\" pitch=\"-3%\"> ठीक है| Amey जी मैं बताना चाहुँगी, की मैं इस वक़्त आपके नज़दीकी ओपो service center और उसकी appointment लेने में, आपके device repair के बारे में, और ओपो फोन के नए parts की price और availability बताने में, आपकी सहायता कर सकती हूँ| कृपया मुझे बताएं, आप इनमें से कौनसी जानकारी चाहेंगे? </prosody></speak>",
            "call_uuid": "bf2f29fc-9218-40a4-a199-6f959753594a",
            "current_intent": "",
            "current_state": "MAIN_MENU",
            "set_intent": {
                "_spare_part_price_collect_oppo_model_name_": "spare_part_price",
                "_spare_part_price_collect_oppo_spare_part_": "spare_part_price"
            },
            "uuid": "e4111581-6bf1-4523-87cb-b2a7d16cde3f",
            "virtual_number": "ABC"
        },
        "history": [
            {
                "entities": [],
                "intents": [
                    {
                        "alternative_index": 0,
                        "name": "_confirm_",
                        "parsers": [
                            "ABCMeta"
                        ],
                        "score": 0.99969,
                        "slots": []
                    }
                ],
                "version": "1.0.6"
            },
            {
                "entities": [],
                "intents": [
                    {
                        "alternative_index": 0,
                        "name": "_cancel_",
                        "parsers": [
                            "ABCMeta"
                        ],
                        "score": 0.99936,
                        "slots": []
                    }
                ],
                "version": "1.0.6"
            }
        ],
        "intents_info": [
            {
                "constraint": {},
                "name": "spare_part_price",
                "slots": [
                    {
                        "name": "oppo_model_name",
                        "type": [
                            "oppo_model_name"
                        ]
                    },
                    {
                        "name": "oppo_spare_part",
                        "type": [
                            "oppo_spare_part"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "book_appointment",
                "slots": [
                    {
                        "name": "booking_time",
                        "type": [
                            "date",
                            "time",
                            "datetime"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "inform_city",
                "slots": [
                    {
                        "name": "city_name",
                        "type": [
                            "city"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "inform_delivery_mode",
                "slots": [
                    {
                        "name": "delivery_mode",
                        "type": [
                            "delivery_mode"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "inform_model_name",
                "slots": [
                    {
                        "name": "oppo_model_name",
                        "type": [
                            "oppo_model_name"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "inform_spare_part",
                "slots": [
                    {
                        "name": "oppo_model_name",
                        "type": [
                            "oppo_model_name"
                        ]
                    },
                    {
                        "name": "oppo_spare_part",
                        "type": [
                            "oppo_spare_part"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "inform_time_slot",
                "slots": [
                    {
                        "name": "booking_time",
                        "type": [
                            "date",
                            "time",
                            "datetime"
                        ]
                    }
                ]
            },
            {
                "constraint": {},
                "name": "spare_part_availability",
                "slots": [
                    {
                        "name": "oppo_model_name",
                        "type": [
                            "oppo_model_name"
                        ]
                    },
                    {
                        "name": "oppo_spare_part",
                        "type": [
                            "oppo_spare_part"
                        ]
                    }
                ]
            }
        ],
        "short_utterance": {},
        "text": "मुझे कल 5:00 से 5:00 से 6:00 तक का बुकिंग चाहिए"
}

"""

alternatives = {
	"actual_format": ".raw",
	"actual_path": "/2021-12-01/6f72bd4f-a466-4500-b848-fef6534ea224/4b59763e-99a8-4f16-bfac-4e2a2f849c84.raw",
	"alternatives": [
                {
                    "confidence": 0.8029602,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.799338,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.8083113,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.7676532,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.76433295,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.8254566,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.80499095,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.8221362,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.8593594,
                    "transcript": "मुझे बैटरी का प्रिंस चाहिए "
                },
                {
                    "confidence": 0.77530867,
                    "transcript": "मुझे ना कल 5:00 से 5:00 से 6:00 बजे तक का बुकिंग चाहिए"
                }
            ],
	"asr_provider": "google",
	"audio_duration": 4.048,
	"cycle_duration": {
		"asr": 4.98809275,
		"asr_from_source": 0,
		"asr_processing": 0.855125154,
		"asr_to_dest": 0,
		"plute": 5.000657452
	},
	"expected_format": ".flac",
	"language": "en",
	"trigger_name": "_fallback_"
}

url = "http://nlu-router/predict/en/hathway-vasr_internet_not_working/"

response = requests.post(
    "http://127.0.0.1:8088/predict/hi/oppo-v1/",
    json=alternatives,
    # headers={"Content-Type": "application/json; charset=utf-8"},
)


print(response.text)
