import pandas as pd
import ast
import json
from collections import Counter
import argparse

KEEP_COLS = ['alternatives', 'audio_url', 'job_id', 'tag','predicted_intent', 'Error Type', 'Label Noise', 'Multiple Intent']


def list_alternatives(alternatives):
    if alternatives:
        alternatives = json.loads(alternatives)
        if len(alternatives) > 0:
            loose_canon = []
            for _ in alternatives[0]:
                loose_canon.append(_['transcript'])
            return loose_canon
    
    return []


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--output_csv", help = "output_csv")

    args = parser.parse_args()

    df = pd.read_csv(args.input_csv)
    df_misfire = pd.DataFrame(columns=KEEP_COLS)

    for col in KEEP_COLS:
        if col in df.columns:
            df_misfire[col] = df[col]
    
    df_misfire['alternatives'] = df_misfire['alternatives'].apply(lambda x : list_alternatives(x))
    df_misfire.to_csv(args.output_csv, index=False)


"""
Sample Usage
python /root/amey/depository/scripts/misfire-analysis-script.py \
--input_csv=/root/amey/clients/hathway/errors/3849_te_errors.csv \
--output_csv=/root/amey/depository/temp/misfire_analysis.csv \
;
"""