import pandas as pd
import ast
import json
from collections import Counter
import argparse

"""
Description: Code to get a single prediction value from SLU slots. 
Use: SLU can return multiple entity matches (Especially if you use ListSearchParser!). 
Eevee considers only the first match (Irrespective of score). 
Thus, in case of multiple matches, the following code performs a majority-voting logic to get the best match for each entity type.
"""
def get_majority_prediction(pred_value):
    predictions = {}
    result = []
    values = ast.literal_eval(pred_value)
    if isinstance(values, list):
        for slot in values:
            # print(slot)
            if slot['type'] in predictions:
                predictions[slot['type']].append(slot['value'])
            else:
                predictions[slot['type']] = [slot['value']]

        for type in predictions:
            predictions[type] = Counter(predictions[type]).most_common(1)[0][0]

        for type in predictions:
            for slot in values:
                if type == slot['type'] and predictions[type] == slot['value']:
                    result.append(slot)
                    break
        
        return json.dumps(result, ensure_ascii=False)
    
    return json.dumps([])

"""
Description: Code to extract 'tag' of entity_type
"""
def get_entity_labels(pred_value):
    predictions = {}
    values = ast.literal_eval(pred_value)
    predictions = {}
    if isinstance(values, list):
        for slot in values:
            # print(slot)
            if slot['type'] in predictions:
                predictions[slot['type']].append(slot['text'])
            else:
                predictions[slot['type']] = [slot['text']]

        for type in predictions:
            predictions[type] = Counter(predictions[type]).most_common(1)[0][0]

        return json.dumps(predictions, ensure_ascii=False)
    return None


"""
Function to remove leading and trailing spaces from values.
"""
def clean_entity(entities: str):
    if isinstance(entities, str):
        entities = json.loads(entities)
        for entity in entities:
            if 'text' in entity.keys():
                entity['text'] = entity['text'].strip()
            
            if 'type' in entity.keys():
                entity['type'] = entity['type'].strip()
            
            if 'value' in entity.keys():
                entity['value'] = entity['value'].strip()

        return json.dumps(entities, ensure_ascii=False)

    return entities

def lowercase_entities(entities: str):
    if isinstance(entities, str):
        entities = json.loads(entities)
        for entity in entities:
            if 'text' in entity.keys():
                entity['text'] = entity['text'].lower()
            
            if 'type' in entity.keys():
                entity['type'] = entity['type'].lower()
            
            if 'value' in entity.keys():
                entity['value'] = entity['value'].lower()

        return json.dumps(entities, ensure_ascii=False)

    return entities

def replace_underscores(entities: str):
    if isinstance(entities, str):
        entities = json.loads(entities)
        for entity in entities:
            if 'text' in entity.keys():
                entity['text'] = entity['text'].replace('_','')

            if 'type' in entity.keys():
                entity['type'] = entity['type'].replace('_','')

            if 'value' in entity.keys():
                entity['value'] = entity['value'].replace('_','')

        return json.dumps(entities, ensure_ascii=False)

    return entities

def unwanted_label(entities: str):
    unwanted_labels = []
                    # 'sms',
                    # 'whatsapp',
                    # 'f1plus', 'f19proplus','find7','findx2','mirror3','mainboard','mainboardtopcoverassembly','reno3pro', 'camerarear','f9','neo3','r1','f5pro','r1','n1mini',
                    # 'slideassembly', 'real camera', 'real camera',
                    # 'touch display', 'fingerprintsensor', 'o', 'simcard', 'antennaboard', 'back cover','19','f1 plus', 'mainboardtopcoverassembly','f3 plus', 'f19 pro', 'f19 pro plus', 'f11 pro', 'f5 youth', 'f5 pro']
    if isinstance(entities, str):
        entities = json.loads(entities)
        for entity in entities:
            if 'text' in entity.keys():
                if entity['text'] in unwanted_labels:
                    return True
            if 'type' in entity.keys():
                if entity['type'] in unwanted_labels:
                    return True
            if 'value' in entity.keys():
                if entity['value'] in unwanted_labels:
                    return True
        return False

    return entities
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help = "input_csv")
    parser.add_argument("--output_csv", help = "output_csv")
    parser.add_argument("--colname", help = "colname")
    parser.add_argument("--replace_underscore", help="replace_underscore")
    parser.add_argument("--fill_entity_label", help="fill_entity_label")
    parser.add_argument("--remove_unwanted_labels", help="remove_unwanted_labels")

    args = parser.parse_args()

    
    if args.replace_underscore:
        replace_underscore = True
    else:
        replace_underscore = False

    if args.fill_entity_label:
        fill_entity_label = True
    else:
        fill_entity_label = False

    if args.remove_unwanted_labels:
        remove_unwanted_labels = True
    else:
        remove_unwanted_labels = False
        
    df = pd.read_csv(args.input_csv)
    if not args.colname in df.columns:
        raise Exception
    else:
        print(f'\nProcessing col: {args.colname}')
        df[args.colname] = df[args.colname].apply(lambda x : get_majority_prediction(x))
        df[args.colname] = df[args.colname].apply(lambda x : clean_entity(x))
        df[args.colname] = df[args.colname].apply(lambda x : lowercase_entities(x))

        if replace_underscore:
            df[args.colname] = df[args.colname].apply(lambda x : replace_underscores(x))

        if fill_entity_label:
            df['entity_label'] = df[args.colname].apply(lambda x : get_entity_labels(x))

        if remove_unwanted_labels:
            df['unwanted_label'] = df[args.colname].apply(lambda x : unwanted_label(x))
            df = df[df.unwanted_label == False]

        df.to_csv(args.output_csv, index=False)
        print(f'\nDone. Saved to: {args.output_csv}')



"""
Sample Run:
python /root/amey/depository/scripts/preprocess.py --input_csv=/root/amey/depository/datasets/MLWR/tagged_data.csv  --output_csv=/root/amey/depository/datasets/MLWR/tagged_data.csv  --colname=entities --replace_underscore=True --fill_entity_label=True --remove_unwanted_labels=True
"""