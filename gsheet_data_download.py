import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import argparse
import sys
sys.path.insert(1, '/root/amey/depository/scripts')
from utils_main_new import make_alternatives

scope = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']
creds_file = '/root/amey/creds/google-api.json'
creds = ServiceAccountCredentials.from_json_keyfile_name(creds_file,scope)
client = gspread.authorize(creds)


def process_gsheet_data(data, data_col = 'English', tag_col = 'tag'):
    df = pd.DataFrame.from_dict(data)
    if not data_col in df.columns or not tag_col in df.columns:
        print('Invalid Sheet Format', df.columns, data_col, tag_col)
        return df
    df = df[[data_col, tag_col]]
    print(f'Original Data: {df.shape}')
    df = df.dropna()
    df = df[df[data_col] != None]
    df = df[df[data_col] != '']
    
    print(f'After Dropping nan Data: {df.shape}')
    return df

def aggregate_gsheet_worksheets(gsheet_file, client, data_col = 'English', tag_col = 'tag', skip_first = 0):

    df_aggregated = pd.DataFrame()

    sh  = client.open(gsheet_file)
    worksheet_list = sh.worksheets()
    
    if int(skip_first) <= len(worksheet_list) and len(worksheet_list) != 0:
        worksheet_list = worksheet_list[int(skip_first):]
    else:
        print('skip_first skipping all worksheets!')

    for worksheet in worksheet_list:
        print(f"Downloading Sheet: {worksheet.title}")
        data = worksheet.get_all_records()
        data = process_gsheet_data(data, data_col = data_col, tag_col=tag_col)
        df_aggregated = pd.concat([df_aggregated,data], ignore_index=True)

    df_aggregated = df_aggregated.dropna()
    df_aggregated = df_aggregated.reset_index()
    return df_aggregated

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--gsheet_file", help = "gsheet_file")
    parser.add_argument("--data_col", help = "data_col")
    parser.add_argument("--tag_col", help = "tag_col")
    parser.add_argument("--output_csv", help = "output_csv")
    parser.add_argument("--skip_first", help = "skip_first")

    args = parser.parse_args()

    df_aggregated = pd.DataFrame()
    df_aggregated = aggregate_gsheet_worksheets(args.gsheet_file, client, data_col = args.data_col, tag_col = args.tag_col, skip_first = args.skip_first)
    df_aggregated.to_csv('/root/amey/depository/temp/agg.csv',index=False)
    df_result = make_alternatives(df_aggregated, colname=args.data_col, savepath=args.output_csv)
    print(df_result.head())
    print(f"\nSaved to {args.output_csv}.")


# Sample Usage
"""

python /root/amey/depository/scripts/gsheet_data_download.py \
--tag_col=tag \
--data_col=English  \
--gsheet_file=Gold-Dataset-Vodafone \
--output_csv=/root/amey/clients/vodafone/datasets/vodafone_gold_en.csv \
--skip_first=1 \
;

"""