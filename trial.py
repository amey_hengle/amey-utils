import pandas as pd
import json
from tqdm import tqdm
import json
import traceback
from typing import Any, List, Optional

import pandas as pd
from loguru import logger
from tqdm import tqdm

import dialogy.constants as const
from dialogy.base import Guard, Input, Output, Plugin
from dialogy.utils import normalize

import ast

def transform(training_data: pd.DataFrame) -> pd.DataFrame:
    training_data["use"] = True
    # logger.debug(f"Transforming dataset via {self.__class__.__name__}")
    for i, row in tqdm(training_data.iterrows(), total=len(training_data)):
        asr_output = None
        try:
            asr_output = json.loads(row['alternatives'])

            if isinstance(asr_output, list):
                if len(asr_output) == 2:
                    if isinstance(asr_output[0],list) and isinstance(asr_output[1],list):
                        print(asr_output)
                        asr_output = asr_output[0][:5] + asr_output[0][:5]

            # print(asr_output)
            if asr_output and (merged_asr_ouptut := merge_asr_output(asr_output)):
                training_data.loc[i, 'merged'] = merged_asr_ouptut[0]
            else:
                training_data.loc[i, "use"] = False
        except Exception as error:  # pylint: disable=broad-except
            training_data.loc[i, "use"] = False
            logger.error(f"{error} -- {asr_output}\n{traceback.format_exc()}")

    return training_data

def merge_asr_output(utterances: Any) -> List[str]:
    """
    .. _merge_asr_output:

    Join ASR output to single string.

    This function provides a merging strategy for n-best ASR transcripts by
    joining each transcript, such that:

    - each sentence end is marked by " </s>" and,
    - sentence start marked by " <s>".

    The key "transcript" is expected in the ASR output, the value of which would be operated on
    by this function.

    The normalization is done by :ref:`normalize<normalize>`

    :param utterances: A structure representing ASR output. We support only:

        1. :code:`List[str]`
        2. :code:`List[List[str]]`
        3. :code:`List[List[Dict[str, str]]]`
        4. :code:`List[Dict[str, str]]`

    :type utterances: Any
    :return: Concatenated string, separated by <s> and </s> at respective terminal positions of each sentence.
    :rtype: List[str]
    :raises: TypeError if transcript is missing in cases of :code:`List[List[Dict[str, str]]]` or
        :code:`List[Dict[str, str]]`.
    """
    try:
        transcripts: List[str] = normalize(utterances)
        invalid_transcript = len(transcripts) == 1 and any(
            token.lower() in transcripts for token in const.INVALID_TOKENS
        )
        if invalid_transcript or not transcripts:
            return []
        else:
            return ["<s> " + " </s> <s> ".join(transcripts) + " </s>"]
    except TypeError as type_error:
        raise TypeError("`transcript` is expected in the ASR output.") from type_error


def make_data_column_uniform(data_frame: pd.DataFrame) -> None:
    if 'alternatives' in data_frame.columns:
        column = 'alternatives'
    elif 'data' in data_frame.columns:
        column = 'data'
    else:
        raise ValueError(
            f"Expected one of {'alternatives'}, {'data'} to be present in the dataset."
        )
    data_frame.rename(columns={column: 'alternatives'}, inplace=True)

    for i, row in tqdm(
        data_frame.iterrows(), total=len(data_frame), desc="Fixing data structure"
    ):
        if isinstance(row['alternatives'], str):
            data = json.load(row['alternatives'])
            if isinstance(data, str):
                raise Exception

            if len(data) == 2:
                print(data)
                if isinstance(data[0],list) and isinstance(data[1],list):
                    temp = data[0][:5] + data[1][:5]
                    print(temp)
                    data = temp
                        
            if 'alternatives' in data:
                data_frame.loc[i, 'alternatives'] = json.dumps(
                    data['alternatives']
                )

    return data_frame

df = pd.read_csv('/root/amey/experiments/vodafone-contextual-slu/train_hi.csv')
temp = df.sample(1)
temp = make_data_column_uniform(temp)
train = transform(temp)

