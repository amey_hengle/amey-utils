from utils_main_new import list_alternatives, read_multiple_df, make_alternatives, read_yaml, read_multiple_df, make_confusion_matrix, get_predictions_from_json, standardize_intent_name, avg_transcript_len, undersample_by_label, preprocess_df, dialogy_to_xlingual, check_transcript_noise
import pandas as pd

yaml = read_yaml('/root/amey/clients/hathway/configs/alias.yaml')
intents_allowed = yaml['_cancel_'] + yaml['_confirm_']


df = read_multiple_df('/root/amey/depository/datasets/csv/hathway')
print(df.shape)

df = df[df.tag.isin(intents_allowed)]
print(df.shape)


df.to_csv('/root/amey/clients/hathway/contextual_smalltalk.csv',index=False)
