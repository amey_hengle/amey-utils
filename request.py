import json
import requests
import time
import pandas as pd
from tqdm.notebook import tqdm_notebook
from tqdm import tqdm
import re
from pprint import pprint
# from locust import HttpUser, task, between



plute_request = {
	"alternatives": 
    [
            {
                "confidence": 0.8759079,
                "transcript": "प्रीपेड में बदलना है"
            },
            {
                "confidence": 0.77926284,
                "transcript": "क्रिकेट में बदलना है"
            },
            {
                "confidence": 0.8501117,
                "transcript": "प्रिंटेड में बदलना है"
            },
            {
                "confidence": 0.89115846,
                "transcript": "प्रेग्नेंट में बदलना है"
            },
            {
                "confidence": 0.89457023,
                "transcript": "प्रिंट में बदलना है"
            },
            {
                "confidence": 0.89115846,
                "transcript": "प्रिपेयर्ड में बदलना है"
            },
            {
                "confidence": 0.89457023,
                "transcript": "प्रिवेट में बदलना है"
            },
            {
                "confidence": 0.8637314,
                "transcript": "प्री पेड में बदलना है"
            }
    ],
	"context": {
            "ack_slots": [],
            "asr_provider": "vernacular",
            "bot_response": "<speak><prosody rate=\"96%\" pitch=\"-4%\"> ठीक है! मैं आपकी मदद जरूर करुँगी। कृपया मुझे बताएं, bill payment करने में आपको क्या परेशानी हो रही है? </prosody></speak>",
            "call_uuid": "b671a5fb-be61-4669-bbda-7cfdab157e6e",
            "current_intent": "",
            "current_state": "COF",
            "disposition": "CBK",
            "entity_matching_references": {
                "due_date_unix": 1649635200
            },
            "set_intent": {
                "_barred_": "payment_promise",
                "_bill_paid_collect_payment_date_": "bill_paid",
                "_bill_paid_collect_payment_mode_": "bill_paid",
                "_eligible_": "payment_promise",
                "_insights_fetched_": "payment_promise",
                "_outstanding_balance_": "payment_promise",
                "_partial_payment_reflecting_": "payment_promise",
                "_payment_not_reflecting_": "payment_promise",
                "_successfully_fetched_": "payment_promise",
                "bill_not_paid": "payment_promise",
                "payment_enquiry": "payment_promise"
            },
            "smalltalk": True,
            "uuid": "43ffefcd-83b2-4c0e-9569-add368b0b22d",
            "virtual_number": "+918045389846"
        },
	"intents_info": None,
	"short_utterance": {},
	# "text": "हाँ "
}

target_url = "http://127.0.0.1:8022/predict/hi/vodafone/"
response1 = requests.post(url=target_url, json=plute_request)
print(response1.elapsed)